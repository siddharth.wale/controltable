package com.homesite.controltable.model;

public class CsvData {

    private String var;
    private String gridId;
    private String s2Score;
    private String s2ScoreDirection;
    private String varThresh;
    private String varDirection;

    public CsvData(String var, String s2Score, String s2ScoreDirection, String varThresh, String varDirection) {
        this.var = var;
        this.s2Score = s2Score;
        this.s2ScoreDirection = s2ScoreDirection;
        this.varThresh = varThresh;
        this.varDirection = varDirection;
    }
    public CsvData(String var, String gridId, String s2Score, String s2ScoreDirection, String varThresh, String varDirection) {
        this.var = var;
        this.gridId = gridId;
        this.s2Score = s2Score;
        this.s2ScoreDirection = s2ScoreDirection;
        this.varThresh = varThresh;
        this.varDirection = varDirection;
    }
    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getS2Score() {
        return s2Score;
    }

    public void setS2Score(String s2Score) {
        this.s2Score = s2Score;
    }

    public String getS2ScoreDirection() {
        return s2ScoreDirection;
    }

    public void setS2ScoreDirection(String s2ScoreDirection) {
        this.s2ScoreDirection = s2ScoreDirection;
    }

    public String getVarThresh() {
        return varThresh;
    }

    public void setVarThresh(String varThresh) {
        this.varThresh = varThresh;
    }

    public String getVarDirection() {
        return varDirection;
    }

    public void setVarDirection(String varDirection) {
        this.varDirection = varDirection;
    }

    public String getGridId() {
        return gridId;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }
}