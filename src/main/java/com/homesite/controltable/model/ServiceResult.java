package com.homesite.controltable.model;

import com.homesite.controltable.util.ServiceStatus;

public class ServiceResult {

    private  String message;
    private ServiceStatus status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public ServiceResult() {
    }

    public ServiceResult(String message, ServiceStatus status) {
        this.message = message;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ServiceResult{" +
                "message='" + message + '\'' +
                ", status=" + status +
                '}';
    }
}
