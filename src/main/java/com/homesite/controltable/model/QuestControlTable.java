package com.homesite.controltable.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class QuestControlTable {

    @NotNull
    @NotEmpty
    private String table_name;
    @NotNull
    @NotEmpty
    private String min_uw_tier;
    @NotNull
    @NotEmpty
    private String max_uw_tier;
    @NotNull
    @NotEmpty
    private String state_code;
    @NotNull
    @NotEmpty
    private String uw_company;
    @NotNull
    @NotEmpty
    private String form_code;
    private @NotNull @NotEmpty String ds_id;
    @NotNull
    @NotEmpty
    private String id;
    @NotNull
    @NotEmpty
    private String action;
    @NotNull
    @NotEmpty
    private String effective_date;
    @NotNull
    @NotEmpty
    private String expiration_date;
    @NotNull
    @NotEmpty
    private String stage_two_score;
    @NotNull
    @NotEmpty
    private String dnq_reason;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String has_flat_roof;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String has_wood_stove;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_number_of_claims;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_number_of_claims;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_loss_amount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_loss_amount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_fire_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_fire_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_non_weather_ex_fire_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_non_weather_ex_fire_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_property_age;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_property_age;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_rcsf;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_rcsf;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_naog_claim;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_naog_claim;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_replacement_cost;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_replacement_cost;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_square_footage;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_square_footage;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_trc_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_trc_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_weather_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_weather_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String min_priority_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String max_priority_score;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sale_price;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sale_price_ratio;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String grid_id;


    public QuestControlTable(@NotNull @NotEmpty String min_uw_tier, @NotNull @NotEmpty String max_uw_tier,
                             @NotNull @NotEmpty String state_code, @NotNull @NotEmpty String uw_company,
                             @NotNull @NotEmpty String form_code, @NotNull @NotEmpty String id,
                             @NotNull @NotEmpty String action, @NotNull @NotEmpty String ds_id,
                             @NotNull @NotEmpty String effective_date, @NotNull @NotEmpty String expiration_date) {
        this.min_uw_tier = min_uw_tier;
        this.max_uw_tier = max_uw_tier;
        this.state_code = state_code;
        this.uw_company = uw_company;
        this.id = id;
        this.action = action;
        this.effective_date = effective_date;
        this.expiration_date = expiration_date;
        this.form_code = form_code;
        this.ds_id = ds_id;
    }

    public QuestControlTable(){

    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getMin_uw_tier() {
        return min_uw_tier;
    }

    public void setMin_uw_tier(String min_uw_tier) {
        this.min_uw_tier = min_uw_tier;
    }

    public String getMax_uw_tier() {
        return max_uw_tier;
    }

    public void setMax_uw_tier(String max_uw_tier) {
        this.max_uw_tier = max_uw_tier;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getUw_company() {
        return uw_company;
    }

    public void setUw_company(String uw_company) {
        this.uw_company = uw_company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(String effective_date) {
        this.effective_date = effective_date;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getStage_two_score() {
        return stage_two_score;
    }

    public void setStage_two_score(String stage_two_score) {
        this.stage_two_score = stage_two_score;
    }

    public String getDnq_reason() {
        return dnq_reason;
    }

    public void setDnq_reason(String dnq_reason) {
        this.dnq_reason = dnq_reason;
    }

    public String getHas_flat_roof() {
        return has_flat_roof;
    }

    public void setHas_flat_roof(String has_flat_roof) {
        this.has_flat_roof = has_flat_roof;
    }

    public String getHas_wood_stove() {
        return has_wood_stove;
    }

    public void setHas_wood_stove(String has_wood_stove) {
        this.has_wood_stove = has_wood_stove;
    }

    public String getMin_number_of_claims() {
        return min_number_of_claims;
    }

    public void setMin_number_of_claims(String min_number_of_claims) {
        this.min_number_of_claims = min_number_of_claims;
    }

    public String getMax_number_of_claims() {
        return max_number_of_claims;
    }

    public void setMax_number_of_claims(String max_number_of_claims) {
        this.max_number_of_claims = max_number_of_claims;
    }

    public String getMin_loss_amount() {
        return min_loss_amount;
    }

    public void setMin_loss_amount(String min_loss_amount) {
        this.min_loss_amount = min_loss_amount;
    }

    public String getMax_loss_amount() {
        return max_loss_amount;
    }

    public void setMax_loss_amount(String max_loss_amount) {
        this.max_loss_amount = max_loss_amount;
    }

    public String getMin_fire_score() {
        return min_fire_score;
    }

    public void setMin_fire_score(String min_fire_score) {
        this.min_fire_score = min_fire_score;
    }

    public String getMax_fire_score() {
        return max_fire_score;
    }

    public void setMax_fire_score(String max_fire_score) {
        this.max_fire_score = max_fire_score;
    }

    public String getMin_non_weather_ex_fire_score() {
        return min_non_weather_ex_fire_score;
    }

    public void setMin_non_weather_ex_fire_score(String min_non_weather_ex_fire_score) {
        this.min_non_weather_ex_fire_score = min_non_weather_ex_fire_score;
    }

    public String getMax_non_weather_ex_fire_score() {
        return max_non_weather_ex_fire_score;
    }

    public void setMax_non_weather_ex_fire_score(String max_non_weather_ex_fire_score) {
        this.max_non_weather_ex_fire_score = max_non_weather_ex_fire_score;
    }

    public String getMin_property_age() {
        return min_property_age;
    }

    public void setMin_property_age(String min_property_age) {
        this.min_property_age = min_property_age;
    }

    public String getMax_property_age() {
        return max_property_age;
    }

    public void setMax_property_age(String max_property_age) {
        this.max_property_age = max_property_age;
    }

    public String getMin_rcsf() {
        return min_rcsf;
    }

    public void setMin_rcsf(String min_rcsf) {
        this.min_rcsf = min_rcsf;
    }

    public String getMax_rcsf() {
        return max_rcsf;
    }

    public void setMax_rcsf(String mac_rcsf) {
        this.max_rcsf = mac_rcsf;
    }

    public String getMin_naog_claim() {
        return min_naog_claim;
    }

    public void setMin_naog_claim(String min_naog_claim) {
        this.min_naog_claim = min_naog_claim;
    }

    public String getMax_naog_claim() {
        return max_naog_claim;
    }

    public void setMax_naog_claim(String max_naog_claim) {
        this.max_naog_claim = max_naog_claim;
    }

    public String getMin_replacement_cost() {
        return min_replacement_cost;
    }

    public void setMin_replacement_cost(String min_replacement_cost) {
        this.min_replacement_cost = min_replacement_cost;
    }

    public String getMax_replacement_cost() {
        return max_replacement_cost;
    }

    public void setMax_replacement_cost(String max_replacement_cost) {
        this.max_replacement_cost = max_replacement_cost;
    }

    public String getMin_square_footage() {
        return min_square_footage;
    }

    public void setMin_square_footage(String min_square_footage) {
        this.min_square_footage = min_square_footage;
    }

    public String getMax_square_footage() {
        return max_square_footage;
    }

    public void setMax_square_footage(String max_square_footage) {
        this.max_square_footage = max_square_footage;
    }

    public String getMin_trc_score() {
        return min_trc_score;
    }

    public void setMin_trc_score(String min_trc_score) {
        this.min_trc_score = min_trc_score;
    }

    public String getMax_trc_score() {
        return max_trc_score;
    }

    public void setMax_trc_score(String max_trc_score) {
        this.max_trc_score = max_trc_score;
    }

    public String getMin_weather_score() {
        return min_weather_score;
    }

    public void setMin_weather_score(String min_weather_score) {
        this.min_weather_score = min_weather_score;
    }

    public String getMax_weather_score() {
        return max_weather_score;
    }

    public void setMax_weather_score(String max_weather_score) {
        this.max_weather_score = max_weather_score;
    }


    public String getMin_priority_score() {
        return min_priority_score;
    }

    public void setMin_priority_score(String min_priority_score) {
        this.min_priority_score = min_priority_score;
    }

    public String getMax_priority_score() {
        return max_priority_score;
    }

    public void setMax_priority_score(String max_priority_score) {
        this.max_priority_score = max_priority_score;
    }

    public String getSale_price() {
        return sale_price;
    }

    public void setSale_price(String sale_price) {
        this.sale_price = sale_price;
    }

    public String getSale_price_ratio() {
        return sale_price_ratio;
    }

    public void setSale_price_ratio(String sale_price_ratio) {
        this.sale_price_ratio = sale_price_ratio;
    }

    public String getForm_code() {
        return form_code;
    }

    public void setForm_code(String form_code) {
        this.form_code = form_code;
    }

    public @NotNull @NotEmpty String getDs_id() {
        return ds_id;
    }

    public void setDs_id(@NotNull @NotEmpty String ds_id) {
        this.ds_id = ds_id;
    }

    public String getGrid_id() {
        return grid_id;
    }

    public void setGrid_id(String grid_id) {
        this.grid_id = grid_id;
    }
}