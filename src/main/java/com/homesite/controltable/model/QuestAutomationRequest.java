package com.homesite.controltable.model;

import com.google.gson.Gson;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class QuestAutomationRequest {

    @NotNull
    @NotEmpty
    private String state_code;
    @NotNull
    @NotEmpty
    private String uw_company;

    private String db_schema_ver;
    private String propertyType;
    private String form_code;

    private String ds_id;
    private String min_uw_tier;
    private String max_uw_tier;

    public QuestAutomationRequest(String state_code, String uw_company, String db_schema_ver, String propertyType, String form_code) {
        this.state_code = state_code;
        this.uw_company = uw_company;
        this.db_schema_ver = db_schema_ver;
        this.propertyType = propertyType;
        this.ds_id = ds_id;
        this.form_code = form_code;
    }

    public QuestAutomationRequest(String state_code, String uw_company, String form_code, String ds_id, String min_uw_tier, String max_uw_tier) {
        this.state_code = state_code;
        this.uw_company = uw_company;
        this.form_code = form_code;
        this.ds_id = ds_id;
        this.min_uw_tier = min_uw_tier;
        this.max_uw_tier = max_uw_tier;
    }

    public QuestAutomationRequest() {

    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getDb_schema_ver() {
        return db_schema_ver;
    }

    public void setDb_schema_ver(String db_schema_ver) {
        this.db_schema_ver = db_schema_ver;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getUw_company() {
        return uw_company;
    }

    public void setUw_company(String uw_company) {
        this.uw_company = uw_company;
    }

    public String getForm_code() {
        return form_code;
    }

    public void setForm_code(String form_code) {
        this.form_code = form_code;
    }

    public String getDs_id() {
        return ds_id;
    }

    public void setDs_id(String ds_id) {
        this.ds_id = ds_id;
    }

    public String getMin_uw_tier() {
        return min_uw_tier;
    }

    public void setMin_uw_tier(String min_uw_tier) {
        this.min_uw_tier = min_uw_tier;
    }

    public String getMax_uw_tier() {
        return max_uw_tier;
    }

    public void setMax_uw_tier(String max_uw_tier) {
        this.max_uw_tier = max_uw_tier;
    }

    public static QuestAutomationRequest getQuestAutomationRequest(String jsonStr){
        Gson g = new Gson();
        return g.fromJson(jsonStr, QuestAutomationRequest.class);
    }
}