package com.homesite.controltable.controller;

import com.homesite.controltable.model.QuestAutomationRequest;
import com.homesite.controltable.service.FileNameService;
import com.homesite.controltable.service.HttpResponseService;
import com.homesite.controltable.service.QuestAutomationService;
import com.homesite.controltable.service.impl.FileNameHelper;
import com.homesite.controltable.util.HttpResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ControlTableController {

    private HttpResponseService httpResponseService;
    private QuestAutomationService questAutomationService;
    private FileNameService fileNameService;

    @Autowired
    public ControlTableController(HttpResponseService httpResponseService, QuestAutomationService questAutomationService, FileNameService fileNameService) {
        this.httpResponseService = httpResponseService;
        this.questAutomationService = questAutomationService;
        this.fileNameService = fileNameService;
    }

    @RequestMapping(value = "/questContolTableAutomation", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity generateControlTableJson(@RequestParam("selected_file") MultipartFile multipartFile, @RequestParam("file_info") String questAutomationRequestStr) {
        QuestAutomationRequest automationRequest = QuestAutomationRequest.getQuestAutomationRequest(questAutomationRequestStr);
        String resourse = "";
        String fileName = "";
        try{
            resourse = questAutomationService.convertMultiFileToJSON(automationRequest, multipartFile);
            fileName = fileNameService.getControlFileName(FileNameHelper.FileAction.Create, automationRequest.getState_code());
            return httpResponseService.getHttpResponseEntity(HttpResponseEnum.Success, fileName, resourse);
        }catch (IllegalArgumentException ex){
            resourse = ex.getMessage();
            ex.printStackTrace();
        }
        return httpResponseService.getHttpResponseEntity(HttpResponseEnum.Failed, fileName, resourse);
    }
}
