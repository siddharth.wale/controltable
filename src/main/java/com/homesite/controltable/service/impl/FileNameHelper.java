package com.homesite.controltable.service.impl;

import com.homesite.controltable.service.FileNameService;
import org.springframework.stereotype.Service;

@Service
public class FileNameHelper implements FileNameService {
    public enum FileAction {
        Create,
        Expire
    }

    /**
     * Get a control file name
     *
     * @param fileAction
     * @param stateCode
     * @return
     */
    public String getControlFileName(FileAction fileAction, String stateCode) {
        if (fileAction == FileAction.Create) {
            return String.format("ITR_No_%1$s_Control_Table.json", stateCode);
        } else {
            return String.format("ITR_No_%1$s_Expire_Control_Tables.json", stateCode);
        }
    }
}

