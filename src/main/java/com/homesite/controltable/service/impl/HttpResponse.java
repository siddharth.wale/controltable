package com.homesite.controltable.service.impl;

import com.homesite.controltable.service.HttpResponseService;
import com.homesite.controltable.util.HttpResponseEnum;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class HttpResponse implements HttpResponseService {
    @Override
    public ResponseEntity getHttpResponseEntity(HttpResponseEnum status, String fileName, String resource) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("access-control-expose-headers", "Content-Disposition");//for CORS, otherwise cannot share the headers.
        if(status == HttpResponseEnum.Success) {
            headers.set(HttpHeaders.CONTENT_DISPOSITION, fileName);

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .headers(headers)
                    .body(resource);
        }

        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(resource);
    }
}
