package com.homesite.controltable.service.impl;

import com.homesite.controltable.model.CsvData;
import com.homesite.controltable.model.QuestControlTable;
import com.homesite.controltable.util.QuestControlTableConstants;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.List;

@Service
public class ControlTableRepository {

    DecimalFormat fourteenDecimal = new DecimalFormat("0.00000000000000");
    DecimalFormat twoDecimal = new DecimalFormat("0.00");

    public QuestControlTable getBaseline(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.Baseline);
        questControlTable.setDnq_reason(QuestControlTableConstants.BaseLineDnq);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        return questControlTable;
    }

    public QuestControlTable getFlatRoof(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.FlatRoof);
        questControlTable.setHas_flat_roof("1");
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setDnq_reason(QuestControlTableConstants.FlatRoofThresholdDnq);
        } else {
            questControlTable.setDnq_reason(QuestControlTableConstants.FlatRoofDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getWoodStove(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.WoodStove);
        questControlTable.setHas_wood_stove("1");
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setDnq_reason(QuestControlTableConstants.WoodStoveThresholdDnq);
        } else {
            questControlTable.setDnq_reason(QuestControlTableConstants.WoodStoveDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getClaim(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setTable_name(QuestControlTableConstants.Claim);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_number_of_claims(csvData.getVarThresh());
            questControlTable.setMax_number_of_claims("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.ClaimsThresholdDnq);
        } else {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("Prior_Clm_Any")) {
                questControlTable.setMin_number_of_claims(csvData.getVarThresh());
                int max = Integer.parseInt(csvData1.getVarThresh()) - 1;
                questControlTable.setMax_number_of_claims(Integer.toString(max));
                questControlTable.setDnq_reason(QuestControlTableConstants.ClaimDnq);
            }
        }
        return questControlTable;
    }

    public QuestControlTable getAOGClaim(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setTable_name(QuestControlTableConstants.AOGClaim);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_number_of_claims(csvData.getVarThresh());
            questControlTable.setMax_number_of_claims("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.AOGClaimThresholdDnq);
        } else {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("Prior_Clm_AOG")) {
                questControlTable.setMin_number_of_claims(csvData.getVarThresh());
                int max = Integer.parseInt(csvData1.getVarThresh()) - 1;
                questControlTable.setMax_number_of_claims(Integer.toString(max));
                questControlTable.setDnq_reason(QuestControlTableConstants.AOGClaimDnq);
            }
        }
        return questControlTable;
    }

    public QuestControlTable getNAOGClaim(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setTable_name(QuestControlTableConstants.NAOGClaim);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_number_of_claims(csvData.getVarThresh());
            questControlTable.setMax_number_of_claims("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.NAOGClaimThresholdDnq);
        } else {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("Prior_Clm_NAOG")) {
                questControlTable.setMin_number_of_claims(csvData.getVarThresh());
                int max = Integer.parseInt(csvData1.getVarThresh()) - 1;
                questControlTable.setMax_number_of_claims(Integer.toString(max));
                questControlTable.setDnq_reason(QuestControlTableConstants.NAOGClaimDnq);
            }
        }
        return questControlTable;
    }

    public QuestControlTable getFireLoss(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.FireLoss);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_loss_amount("10001");
            questControlTable.setMax_loss_amount("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.FireLossThresholdDnq);
        } else {
            questControlTable.setMin_loss_amount("10001");
            questControlTable.setMax_loss_amount("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.FireLossDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getPropertyAge(QuestControlTable questControlTable, List<CsvData> csvDataList, String propertyType, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setTable_name(QuestControlTableConstants.PropertyAge);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        if (!(QuestAutomationServiceImpl.IsTwoNamesEqual(csvData1.getVar(), "AgeOfHomeOrWiring") || QuestAutomationServiceImpl.IsTwoNamesEqual(csvData1.getVar(), "AgeOfWiring") || QuestAutomationServiceImpl.IsTwoNamesEqual(csvData1.getVar(), "AgeOfHome"))) {
            questControlTable.setMin_property_age(parseMinExponential(csvData.getVarThresh()));
            questControlTable.setMax_property_age("2147483647");
            if (propertyType.equals("home")) {
                questControlTable.setDnq_reason(QuestControlTableConstants.PropertyAgeOfHomeThresholdDnq);
            } else if (propertyType.equals("wiring")) {
                questControlTable.setDnq_reason(QuestControlTableConstants.PropertyAgeOfWiringThresholdDnq);
            }
            return questControlTable;
        } else {
            int min = (int) Double.parseDouble(csvData.getVarThresh());
            questControlTable.setMin_property_age(Integer.toString(min));
            if (i + 1 < csvDataList.size() && (csvData1.getVar().equals("AgeOfHomeOrWiring") || csvData1.getVar().equals("AgeOfWiring") || csvData1.getVar().equals("AgeOfHome"))) {
                questControlTable.setMax_property_age(parseMaxExponential(csvData1.getVarThresh()));
            }
            if (propertyType.equals("home")) {
                questControlTable.setDnq_reason(QuestControlTableConstants.PropertyAgeOfHomeDnq);
            } else if (propertyType.equals("wiring")) {
                questControlTable.setDnq_reason(QuestControlTableConstants.PropertyAgeOfWiringDnq);
            }
            return questControlTable;
        }
    }

    public QuestControlTable getReplacementCost(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.ReplacementCost);
        questControlTable.setMin_replacement_cost(parseMinExponential(csvData.getVarThresh()));
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            if (questControlTable.getMin_replacement_cost().equals("1250001")){
                questControlTable.setDnq_reason(QuestControlTableConstants.ReplacementCostNewThresholdDnq);
            }else {
                questControlTable.setDnq_reason(QuestControlTableConstants.ReplacementCostThresholdDnq);
            }
            questControlTable.setMax_replacement_cost("2147483647");

            return questControlTable;
        } else {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("PropertyRC")) {
                questControlTable.setMax_replacement_cost(parseMaxExponential(csvData1.getVarThresh()));
            }
            questControlTable.setDnq_reason(QuestControlTableConstants.ReplacementCostDnq);
            return questControlTable;
        }
    }

    public QuestControlTable getSquareFoot(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.SquareFoot);
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_square_footage(csvData.getVarThresh());
            questControlTable.setMax_square_footage("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.SquareFootThresholdDnq);
            return questControlTable;
        } else {
            questControlTable.setMin_square_footage(csvData.getVarThresh());
            int max = Integer.parseInt(csvData1.getVarThresh()) - 1;
            questControlTable.setMax_square_footage(Integer.toString(max));
            questControlTable.setDnq_reason(QuestControlTableConstants.SquareFootDnq);
            return questControlTable;
        }
    }

    public QuestControlTable getRCSF(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i < csvDataList.size()) {
            csvData1 = csvDataList.get(i - 1);
        }
        String min = twoDecimal.format(Double.parseDouble(csvData1.getVarThresh()) + 0.01);
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.RCSF);
        if (csvData.getVarDirection().equals("<=") && (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000"))) {
            questControlTable.setMax_rcsf(twoDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            questControlTable.setMin_rcsf("0.00");
            questControlTable.setDnq_reason(QuestControlTableConstants.RCSFThresholdDnq);
        }else{
            questControlTable.setMax_rcsf(twoDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            questControlTable.setMin_rcsf(min);
            questControlTable.setDnq_reason(QuestControlTableConstants.RCSFDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getRcsfNaogClaims(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.RCSFNAOGClaim);
        questControlTable.setMin_rcsf("0.00");
        questControlTable.setMax_rcsf("124.99");
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setMin_naog_claim(csvData.getVarThresh());
            questControlTable.setMax_naog_claim("2147483647");
            questControlTable.setDnq_reason(QuestControlTableConstants.RCSFNAOGClaimThresholdDnq);
            return questControlTable;
        } else {
            questControlTable.setMin_naog_claim(csvData.getVarThresh());
            int max = Integer.parseInt(csvData1.getVarThresh()) - 1;
            questControlTable.setMax_naog_claim(Integer.toString(max));
            questControlTable.setDnq_reason(QuestControlTableConstants.RCSFNAOGClaimDnq);
            return questControlTable;
        }
    }

    public QuestControlTable getWeatherScore(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.WeatherScore);
        if (i + 1 < csvDataList.size() && csvData1.getVar().equals("S1_Weather_Score")) {
            questControlTable.setMin_weather_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            String max = fourteenDecimal.format(Double.parseDouble(csvData1.getVarThresh()) - 0.00000000000001);
            questControlTable.setMax_weather_score(max);
        } else {
            questControlTable.setMin_weather_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            questControlTable.setMax_weather_score("1000.00000000000000");
        }
        questControlTable.setDnq_reason(QuestControlTableConstants.WeatherScoreDnq);
        return questControlTable;
    }

    public QuestControlTable getNonWeatherExFireScore(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.NonWeatherExFireScore);
        if (csvData.getGridId() == null || csvData.getGridId().trim().isEmpty()) {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("S1_NWxFire_Score")) {
                questControlTable.setMin_non_weather_ex_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
                String max = fourteenDecimal.format(Double.parseDouble(csvData1.getVarThresh()) - 0.00000000000001);
                questControlTable.setMax_non_weather_ex_fire_score(max);
            } else {
                questControlTable.setMin_non_weather_ex_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
                questControlTable.setMax_non_weather_ex_fire_score("1000.00000000000000");
            }
        } else {
            if (i + 1 < csvDataList.size() && csvData1.getVar().equals("S1_NWxFire_Score") && csvData.getGridId().equals(csvData1.getGridId())) {
                questControlTable.setMin_non_weather_ex_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
                String max = fourteenDecimal.format(Double.parseDouble(csvData1.getVarThresh()) - 0.00000000000001);
                questControlTable.setMax_non_weather_ex_fire_score(max);
                questControlTable.setGrid_id(csvData.getGridId());
            }else {
                questControlTable.setMin_non_weather_ex_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
                questControlTable.setMax_non_weather_ex_fire_score("1000.00000000000000");
                questControlTable.setGrid_id(csvData.getGridId());
            }
        }

        questControlTable.setDnq_reason(QuestControlTableConstants.NonWeatherExFireScoreDnq);
        if(questControlTable.getGrid_id() !=null){
            System.out.println(questControlTable.getGrid_id());
        }
        return questControlTable;
    }

    public QuestControlTable getFireScore(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.FireScore);
        if (i + 1 < csvDataList.size() && csvData1.getVar().equals("S1_Fire_Score")) {
            questControlTable.setMin_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            String max = fourteenDecimal.format(Double.parseDouble(csvData1.getVarThresh()) - 0.00000000000001);
            questControlTable.setMax_fire_score(max);
        } else {
            questControlTable.setMin_fire_score(fourteenDecimal.format(Double.parseDouble(csvData.getVarThresh())));
            questControlTable.setMax_fire_score("1000.00000000000000");
        }
        questControlTable.setDnq_reason(QuestControlTableConstants.FireScoreDnq);
        return questControlTable;
    }

    public QuestControlTable getTrcScore(QuestControlTable questControlTable, List<CsvData> csvDataList, int i) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i + 1 < csvDataList.size()) {
            csvData1 = csvDataList.get(i + 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.TRCScore);
        if (i + 1 < csvDataList.size() && csvData1.getVar().equals("TRC_Score")) {
            if (csvData.getVarDirection().equals("<")) {
                int max = Integer.parseInt(csvData.getVarThresh()) - 1;
                questControlTable.setMax_trc_score(Integer.toString(max));
                questControlTable.setMin_trc_score(csvData1.getVarThresh());
            } else if (csvData.getVarDirection().equals("<=")) {
                questControlTable.setMax_trc_score(csvData.getVarThresh());
                int min = Integer.parseInt(csvData1.getVarThresh()) + 1;
                questControlTable.setMin_trc_score(Integer.toString(min));
            }
        } else {
            if (csvData.getVarDirection().equals("<")) {
                int max = Integer.parseInt(csvData.getVarThresh()) - 1;
                questControlTable.setMax_trc_score(Integer.toString(max));
                questControlTable.setMin_trc_score("0");
            } else if (csvData.getVarDirection().equals("<=")) {
                questControlTable.setMax_trc_score(csvData.getVarThresh());
                questControlTable.setMin_trc_score("0");
            }
        }
        questControlTable.setDnq_reason(QuestControlTableConstants.TRCScoreDnq);
        return questControlTable;
    }

    public QuestControlTable getMLSSalePrice(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.MLSSalePrice);
        questControlTable.setSale_price(csvData.getVarThresh());
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        //ToDo: Need to understand what state two score is to see if we need this logic.
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setDnq_reason(QuestControlTableConstants.MLSSalePriceDnqThresholdDnq);
        } else {
            questControlTable.setDnq_reason(QuestControlTableConstants.MLSSalePriceDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getMLSSalePriceRatio(QuestControlTable questControlTable, CsvData csvData) {
        questControlTable.setTable_name(QuestControlTableConstants.MLSSalePriceRatio);
        questControlTable.setSale_price_ratio(csvData.getVarThresh());
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        //ToDo: Need to understand what state two score is to see if we need this logic.
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setDnq_reason(QuestControlTableConstants.MLSSalePriceRatioDnqThresholdDnq);
        } else {
            questControlTable.setDnq_reason(QuestControlTableConstants.MLSSalePriceRatioDnq);
        }
        return questControlTable;
    }

    public QuestControlTable getPriorityScore(QuestControlTable questControlTable, List<CsvData> csvDataList, int i, int count) {
        CsvData csvData = csvDataList.get(i);
        CsvData csvData1 = null;
        if (i <= csvDataList.size()) {
            csvData1 = csvDataList.get(i - 1);
        }
        questControlTable.setStage_two_score(fourteenDecimal.format(Double.parseDouble(csvData.getS2Score())));
        questControlTable.setTable_name(QuestControlTableConstants.PriorityScore);

        if (i + 1 <= csvDataList.size() && csvData.getVar().equals("PriorityScore")) {
            if (csvData.getVarDirection().equals("<=")) {
                int max = Integer.parseInt(csvData.getVarThresh());
                questControlTable.setMax_priority_score(Integer.toString(max));
                if (count == 0) {
                    questControlTable.setMin_priority_score("0");
                    count++;
                } else {
                    int min = Integer.parseInt(csvData1.getVarThresh()) + 1;
                    questControlTable.setMin_priority_score(Integer.toString(min));
                }
            }
        }
        if (questControlTable.getStage_two_score().equals("0.00000000000000") || questControlTable.getStage_two_score().equals("99.00000000000000") ) {
            questControlTable.setDnq_reason(QuestControlTableConstants.PriorityScoreThresholdDnq);
        } else {
            questControlTable.setDnq_reason(QuestControlTableConstants.PriorityScoreDnq);
        }
        return questControlTable;
    }



    public String parseMinExponential(String s) {
        int min = (int) Double.parseDouble(s);
        return Integer.toString(min);
    }

    public String parseMaxExponential(String s) {
        int max = (int) Double.parseDouble(s) - 1;
        return Integer.toString(max);
    }
}