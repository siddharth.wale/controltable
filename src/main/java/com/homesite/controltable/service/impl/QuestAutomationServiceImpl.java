package com.homesite.controltable.service.impl;

import com.homesite.controltable.model.CsvData;
import com.homesite.controltable.model.QuestAutomationRequest;
import com.homesite.controltable.model.QuestControlTable;
import com.homesite.controltable.service.QuestAutomationService;
import com.homesite.controltable.service.SaveService;
import com.homesite.controltable.util.CsvReader;
import com.homesite.controltable.util.JsonUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class QuestAutomationServiceImpl implements QuestAutomationService {

    @Autowired
    SaveService saveService;

    @Autowired
    JsonUtil jsonUtil;

    @Autowired
    CsvReader csvReader;

    @Autowired
    ControlTableRepository controlTableRepository;

    public String convertMultiFileToJSON(QuestAutomationRequest questAutomationRequest, MultipartFile multipartFile) {

        List<CsvData> csvDataList = csvReader.getCsvDataListFromMultipleFile(multipartFile);

        Map<String, Object> map = new HashMap<>();

        String state_code = questAutomationRequest.getState_code();
        String uw_company = questAutomationRequest.getUw_company();
        String ds_id = questAutomationRequest.getDs_id();
        try {
            map = getControlTableMap(csvDataList, state_code, uw_company,
                    questAutomationRequest.getForm_code(), ds_id, questAutomationRequest.getPropertyType(),
                    questAutomationRequest.getDb_schema_ver());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONObject jsonObj = jsonUtil.convertMapToJSON(map);
        if (csvDataList.size() != jsonObj.length())
            throw new IllegalArgumentException(String.format("CSV size: %1$s, JSON size: %2$s", csvDataList.size(), jsonObj.length() - 1));

        return jsonObj.toString(4);
    }


    public String createGUID() {
        return String.valueOf(java.util.UUID.randomUUID());
    }

    public Map<String, Object> getControlTableMap(List<CsvData> csvDataList, String stateCode, String uwCompany,
                                                  String formCode, String dsId, String propertyType, String dbSchemaVer) {
        Map<String, Object> map = new HashMap<>();
        map.put("db_schema_ver", dbSchemaVer);

        for (int i = 0; i < csvDataList.size(); i++) {
            CsvData csvData = csvDataList.get(i);
            QuestControlTable questControlTable = new QuestControlTable("0", "999", stateCode,
                    uwCompany, formCode, createGUID(), "create", dsId, "$Eff_Date",
                    "2050-01-01 00:00:00");

            if (IsTwoNamesEqual(csvData.getVar(), "Baseline")) {
                controlTableRepository.getBaseline(questControlTable, csvData);
            } else if (IsTwoNamesEqual(csvData.getVar(), "RedFlag_FlatRoof")) {
                controlTableRepository.getFlatRoof(questControlTable, csvData);
            } else if (IsTwoNamesEqual(csvData.getVar(), "RedFlag_WoodStove")) {
                controlTableRepository.getWoodStove(questControlTable, csvData);
            } else if (IsTwoNamesEqual(csvData.getVar(), "RedFlag_LargeFire")) {
                controlTableRepository.getFireLoss(questControlTable, csvData);
            } else if (IsTwoNamesEqual(csvData.getVar(), "Prior_Clm_NAOG")) {
                controlTableRepository.getNAOGClaim(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "Prior_Clm_Any")) {
                controlTableRepository.getClaim(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "Prior_Clm_AOG")) {
                controlTableRepository.getAOGClaim(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "PropertyRCSF")) {
                controlTableRepository.getRCSF(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "AgeOfHomeOrWiring") || IsTwoNamesEqual(csvData.getVar(), "AgeOfWiring") || IsTwoNamesEqual(csvData.getVar(), "AgeOfHome")) {
                controlTableRepository.getPropertyAge(questControlTable, csvDataList, propertyType, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "PropertyRC")) {
                controlTableRepository.getReplacementCost(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "PropertySF")) {
                controlTableRepository.getSquareFoot(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "RedFlag_RCSF_NAOG_claims") || IsTwoNamesEqual(csvData.getVar(), "RedFlag_RCSF_PrClm")) {
                controlTableRepository.getRcsfNaogClaims(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "S1_Fire_Score")) {
                controlTableRepository.getFireScore(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "S1_NWxFire_Score")) {
                controlTableRepository.getNonWeatherExFireScore(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "S1_Weather_Score")) {
                controlTableRepository.getWeatherScore(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "TRC_Score") || IsTwoNamesEqual(csvData.getVar(), "TrueRisk_Combo")) {
                controlTableRepository.getTrcScore(questControlTable, csvDataList, i);
            } else if (IsTwoNamesEqual(csvData.getVar(), "PriorityScore")) {
                controlTableRepository.getPriorityScore(questControlTable, csvDataList, i, map.size());
            } else if (IsTwoNamesEqual(csvData.getVar(), "MLS_SalePrice") || IsTwoNamesEqual(csvData.getVar(), "mlsSalePrice")) {
                controlTableRepository.getMLSSalePrice(questControlTable, csvData);
            }else if (IsTwoNamesEqual(csvData.getVar(), "MLS_PriceRatio") || IsTwoNamesEqual(csvData.getVar(), "mlsSalePriceToPriorRatio")) {
                controlTableRepository.getMLSSalePriceRatio(questControlTable, csvData);
            } else {
                continue; //No such file
            }
            map.put(String.valueOf(map.size()), questControlTable);
        }
        return map;
    }

    public static boolean IsTwoNamesEqual(String s1, String s2) {
        String s1Str = s1.toLowerCase().replaceAll("[^a-z]+", "");
        String s2Str = s2.toLowerCase().replaceAll("[^a-z]+", "");
        return s1Str.equals(s2Str);
    }

}
