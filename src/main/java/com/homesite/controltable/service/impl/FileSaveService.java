package com.homesite.controltable.service.impl;

import com.homesite.controltable.service.SaveService;
import com.homesite.controltable.util.FileSaveStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
public class FileSaveService implements SaveService {

    @Override
    public FileSaveStatus save(String path, String content, boolean overwriteFile) {
        FileSaveStatus result;

        try {
            // Check if file exists
            File file = new File(path);
            boolean fileExists = file.exists();

            // 1. file exists and overwriteFile = true: save
            // 2. file does not exist: save
            // 3. file exists and overwrite = false: exists

            if (fileExists && !overwriteFile) {
                result = FileSaveStatus.exists;
            } else {
                // attempt the save
                OutputStream outputStream = new FileOutputStream(path, false);
                byte[] strToBytes = content.getBytes();
                outputStream.write(strToBytes);
                outputStream.close();
                result = FileSaveStatus.saved;
            }
        }catch(Exception ex){
            result = FileSaveStatus.failed;
        }

        return result;
    }
}
