package com.homesite.controltable.service;

import com.homesite.controltable.service.impl.FileNameHelper;

public interface FileNameService {
    String getControlFileName(FileNameHelper.FileAction fileAction, String stateCode);
}
