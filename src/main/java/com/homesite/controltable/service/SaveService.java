package com.homesite.controltable.service;

import com.homesite.controltable.util.FileSaveStatus;

public interface SaveService {
    FileSaveStatus save(String path, String content, boolean overwriteFile);
}