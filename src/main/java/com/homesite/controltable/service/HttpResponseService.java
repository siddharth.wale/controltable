package com.homesite.controltable.service;

import com.homesite.controltable.util.HttpResponseEnum;
import org.springframework.http.ResponseEntity;

public interface HttpResponseService {
    ResponseEntity getHttpResponseEntity(HttpResponseEnum status, String fileName, String resource);
}
