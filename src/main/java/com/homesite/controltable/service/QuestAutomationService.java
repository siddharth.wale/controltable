package com.homesite.controltable.service;

import com.homesite.controltable.model.QuestAutomationRequest;
import org.springframework.web.multipart.MultipartFile;

public interface QuestAutomationService {
    String convertMultiFileToJSON(QuestAutomationRequest questAutomationRequest, MultipartFile multipartFile);
}
