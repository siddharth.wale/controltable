package com.homesite.controltable.util;

public enum FileSaveStatus {
    saved,
    exists,
    failed,
    notAttempted
}