package com.homesite.controltable.util;

public class QuestControlTableConstants {

    //Table Name Constants
    public static final String Baseline ="hs_baseline";
    public static final String FlatRoof ="hs_flat_roof";
    public static final String WoodStove ="hs_wood_stove";
    public static final String Claim ="hs_claim";
    public static final String AOGClaim ="hs_aog_claim";
    public static final String NAOGClaim ="hs_naog_claim";
    public static final String FireLoss ="hs_fire_loss";
    public static final String PropertyAge ="hs_property_age";
    public static final String ReplacementCost ="hs_replacement_cost";
    public static final String SquareFoot ="hs_square_foot";
    public static final String RCSF ="hs_rcsf";
    public static final String RCSFNAOGClaim ="hs_rcsf_naog_claims";
    public static final String WeatherScore ="hs_weather_score";
    public static final String NonWeatherExFireScore ="hs_non_weather_ex_fire_score";
    public static final String FireScore ="hs_fire_score";
    public static final String TRCScore ="hs_trc_score";
    public static final String PriorityScore = "hs_priority_score";
    public static final String MLSSalePrice ="hs_sale_price";
    public static final String MLSSalePriceRatio ="hs_sale_price_ratio";
    public static final String AttributeTable = "hs_attribute";

    //DNQ Reason Constant
    public static final String BaseLineDnq ="UW Model threshold not met";
    public static final String FlatRoofDnq ="UW Model and Flat Roof";
    public static final String FlatRoofThresholdDnq ="Property Has a Flat Roof";
    public static final String WoodStoveThresholdDnq ="Wood must not be the primary heating source.";
    public static final String WoodStoveDnq ="UW Model and Wood Stove";
    public static final String ClaimDnq ="UW Model and Total Claims";
    public static final String ClaimsThresholdDnq ="We're unable to offer a policy based on your claims history.";
    public static final String AOGClaimDnq ="UW Model and AOG Claims";
    public static final String AOGClaimThresholdDnq ="AOG Claims greater than allowable limit";
    public static final String NAOGClaimThresholdDnq ="NAOG Claims greater than allowable limit";
    public static final String NAOGClaimDnq ="UW Model and NAOG Claims";
    public static final String FireLossDnq ="UW Model and Fire Loss >$10,000";
    public static final String FireLossThresholdDnq ="We're unable to offer a policy due to claims history with large fire losses.";
    public static final String PropertyAgeOfHomeDnq ="UW Model and Home Age";
    public static final String PropertyAgeOfWiringDnq ="UW Model and Wiring Age";
    public static final String PropertyAgeOfHomeThresholdDnq ="Age of home is greater than allowable limit";
    public static final String PropertyAgeOfWiringThresholdDnq ="Age of Wiring is greater than allowable limit";
    public static final String ReplacementCostDnq ="UW Model and RC is greater than allowable limit";
    public static final String ReplacementCostThresholdDnq ="The estimated cost to rebuild your home is greater than our limit of $1,100,000.";
    public static final String ReplacementCostNewThresholdDnq ="The estimated cost to rebuild your home is greater than our limit of $1,250,000.";
    public static final String SquareFootDnq ="UW Model and Property SQ FT";
    public static final String SquareFootThresholdDnq ="Property Square Footage is greater than allowable limit";
    public static final String RCSFThresholdDnq ="Replacement cost to square footage ratio is below the minimum";
    public static final String RCSFDnq ="UW Model and RC/SQ FT ratio";
    public static final String RCSFNAOGClaimDnq ="UW Model and RCSF & NAOG Claims";
    public static final String RCSFNAOGClaimThresholdDnq ="Replacement cost to square footage ratio is below the minimum and prior claims";
    public static final String WeatherScoreDnq ="UW Model and Weather Score";
    public static final String NonWeatherExFireScoreDnq ="UW Model and Non-Weather x Fire Score";
    public static final String FireScoreDnq ="UW Model and Fire Score";
    public static final String TRCScoreDnq ="UW Model and Insurance Score";
    public static final String PriorityScoreDnq = "UW Model and Inspection Priority Score";
    public static final String PriorityScoreThresholdDnq = "Inspection Score is lower than allowable limit";

    public static final String MLSSalePriceDnq = "UW Model and Inspection MLS Sale Price";
    public static final String MLSSalePriceDnqThresholdDnq = "Current sales price is less than $30,000.";
    public static final String MLSSalePriceRatioDnq = "UW Model and Inspection MLS Sale Price Ratio";
    public static final String MLSSalePriceRatioDnqThresholdDnq = "Current sales price is less than 50% of the prior sales price.";
}
