package com.homesite.controltable.util;

import com.homesite.controltable.model.CsvData;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Repository
public class CsvReader {

    public static final String COMMA_DELIMITER = ",";

    public List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<String>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(COMMA_DELIMITER);
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }

    public List<CsvData> convertFileToCsvDataList(File file) {
        List<CsvData> csvDataList = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                List<String> list = getRecordFromLine(scanner.nextLine());
                CsvData csvData = null;
                if(list.size()>5){
                    if(list.get(0).trim().isEmpty()
                            && list.get(1).trim().isEmpty()
                            && list.get(2).trim().isEmpty()
                            && list.get(3).trim().isEmpty()
                            && list.get(4).trim().isEmpty()
                            && list.get(5).trim().isEmpty()){
                        break;
                    }
                    csvData  = new CsvData(list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
                }else{
                    if(list.get(0).trim().isEmpty()
                            && list.get(1).trim().isEmpty()
                            && list.get(2).trim().isEmpty()
                            && list.get(3).trim().isEmpty()
                            && list.get(4).trim().isEmpty()){
                        break;
                    }
                    csvData = new CsvData(list.get(0), list.get(1), list.get(2), list.get(3), list.get(4));
                }

                csvDataList.add(csvData);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } /* this is to catch issue that is caused due to not having valid number of the columns in the csv file (this happend while generating for MI state)... csv file should contain valid 5 columns*/
        catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
        return csvDataList;
    }



    public List<CsvData> getCsvDataListFromMultipleFile(MultipartFile file){
        File conFile = new File(file.getOriginalFilename());
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(conFile);
            fos.write(file.getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<CsvData> csvDataList = convertFileToCsvDataList(conFile);
        return csvDataList;
    }
}
