package com.homesite.controltable.util;

import com.homesite.controltable.model.QuestControlTable;
import com.homesite.controltable.model.ServiceResult;
import com.homesite.controltable.service.SaveService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class JsonUtil {

    @Autowired
    SaveService saveService;

    int nodeCount = 0;
    HashMap<String, Object> hashMap = new HashMap<>();

    /**
     * Prepares the hashMap for a new set of controlTable items
     */
    public void clear(){
        hashMap.clear();
        resetNode();
    }

    public void addDbSchemaVer(String schemaVer) {
        String dbKey = "db_schema_ver";
        hashMap.put(dbKey, schemaVer);
    }

    public String getJsonString(){
        JSONObject finalJson = new JSONObject(hashMap);
        String populated = toPrettyFormat(finalJson.toString());
        System.out.println(populated);
        return populated;
    }

    public JSONObject convertMapToJSON(Map<String, Object> hashMapObj){
        JSONObject jsonObj = new JSONObject(hashMapObj);
        return jsonObj;
    }

    public JSONObject convertToJSONObject(){
        JSONObject jsonObj = new JSONObject(hashMap);
        return jsonObj;
    }

    public String convertMapToJSONString(Map<String, Object> hashMapObj){
        JSONObject finalJson = new JSONObject(hashMapObj);
        String populated = toPrettyFormat(finalJson.toString());
        System.out.println(populated);
        return populated;
    }

    public ServiceResult getServiceResult(String outputPath) {
        JSONObject finalJson = new JSONObject(hashMap);
        String populated = toPrettyFormat(finalJson.toString());
        System.out.println(populated);
        FileSaveStatus status;
        if (!outputPath.isEmpty()) {
            status = saveService.save(outputPath, populated, true);
        } else {
            status = FileSaveStatus.notAttempted;
        }
        return getServiceResult(status);
    }

    public String toPrettyFormat(String jsonString) {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        JsonNode tree = null;
        String formattedJson = null;
        try {
            tree = mapper.readTree(jsonString);
            formattedJson = mapper.writeValueAsString(tree);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return formattedJson;
    }

    public ServiceResult getServiceResult(FileSaveStatus status) {
        ServiceResult serviceResult = new ServiceResult();
        switch (status) {
            case failed:
            case notAttempted:
                serviceResult.setMessage("There was an error completing this transaction");
                serviceResult.setStatus(ServiceStatus.Failed);
                break;
            case exists:
                serviceResult.setMessage("This file already exists. please run the request again with a new file name or an overwrite flag as true");
                serviceResult.setStatus(ServiceStatus.Failed);
                break;
            case saved:
                serviceResult.setMessage("Please check the file in the specified path");
                serviceResult.setStatus(ServiceStatus.Success);
                break;
        }
        return serviceResult;
    }

    public void addHashmap(QuestControlTable questControlTable) {
        hashMap.put(Integer.toString(nodeCount), questControlTable);
        nodeCount++;
    }

    public HashMap<String, Object> getHashMap() { return hashMap; }

    public void resetNode(){
        nodeCount = 0;
    }

}