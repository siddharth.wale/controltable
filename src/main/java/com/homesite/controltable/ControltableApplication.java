package com.homesite.controltable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.homesite.controltable"})
public class ControltableApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControltableApplication.class, args);
	}

}
